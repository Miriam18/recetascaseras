
from django.contrib import admin
from .models import ModoPago, ListadoPoductos, PedidosCliente, FormularioContacto

class RecetasAdmin(admin.ModelAdmin):
    list_display=('NombreCliente','Apellidos', 'Telefono', 'Correo', 'Direccion', 'ModoPago', 'NombreProducto')
    search_fields = ['NombreCliente,NombreProducto']
    list_filter=('NombreCliente',)


class DatosContacto(admin.ModelAdmin):
    list_display=('NombreCliente','Apellidos', 'Telefono', 'Correo', 'Descripcion')
    search_fields = ['NombreCliente']
    list_filter=('NombreCliente',)

admin.site.register(ModoPago)
admin.site.register(ListadoPoductos)
admin.site.register(PedidosCliente, RecetasAdmin)
admin.site.register(FormularioContacto, DatosContacto)