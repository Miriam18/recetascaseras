from django import forms
from django.forms import ModelForm
from .models import PedidosCliente, FormularioContacto
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm



class PedidosClienteForm(ModelForm):
 class Meta:
    model =  PedidosCliente
    fields =['NombreCliente','Apellidos', 'Telefono', 'Correo', 'Direccion', 'ModoPago', 'NombreProducto']


class DatosRegistro(UserCreationForm):
    # Ahora el campo username es de tipo email y cambiamos su texto
    first_name = forms.CharField(max_length=50, label='Nombre')
    last_name = forms.CharField(max_length=50, label='Apellidos')
    email = forms.EmailField(max_length=70, label='Correo Electronico')

    class Meta:
        model = User
        fields =['first_name', 'last_name', 'email', 'username', 'password1', 'password2']

class FormularioContactoForm(ModelForm):
 class Meta: 
    model= FormularioContacto 
    fields =['NombreCliente','Apellidos', 'Telefono', 'Correo', 'Descripcion']