from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import authenticate
from django.contrib.auth import logout as do_logout
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login as do_login
from django.contrib.auth.models import User
from django import forms
from django.forms import ModelForm 
from .models import ModoPago, ListadoPoductos, PedidosCliente, FormularioContacto
from .forms import PedidosClienteForm, DatosRegistro, FormularioContactoForm



def base(request):

    return render(request, "core/base.html")

def home(request):
   
    return render(request, "core/home.html")
   
    
def login(request):
    # Creamos el formulario de autenticación vacío
    form = AuthenticationForm()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = AuthenticationForm(data=request.POST)
        # Si el formulario es válido...
        if form.is_valid():
            # Recuperamos las credenciales validadas
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            # Verificamos las credenciales del usuario
            user = authenticate(username=username, password=password)

            # Si existe un usuario con ese nombre y contraseña
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user)
                # Y le redireccionamos a la portada
                return redirect('home')

    # Si llegamos al final renderizamos el formulario
    return render(request, "registration/login.html", {'form': form})    






def register(request):
   
    # Creamos el formulario de autenticación vacío
    form = DatosRegistro()
    if request.method == "POST":
        # Añadimos los datos recibidos al formulario
        form = DatosRegistro(data=request.POST)
        
        # Si el formulario es válido...
        if form.is_valid():
            
            # Creamos la nueva cuenta de usuario
            user = form.save()
            
            
            
            # Si el usuario se crea correctamente 
            if user is not None:
                # Hacemos el login manualmente
                do_login(request, user, backend='django.contrib.auth.backends.ModelBackend')
                # Y le redireccionamos a la portada
                return redirect('home')
    form.fields['username'].help_text = None
    form.fields['password1'].help_text = None
    form.fields['password2'].help_text = None
    
    # Si llegamos al final renderizamos el formulario
    return render(request, "registration/register.html", {'form': form})
    
  

#para cerrar sesion

def logout(request):
    # Finalizamos la sesión
    do_logout(request)
    # Redireccionamos a la portada
    return redirect('/')


def Recetauno(request):
    data= {
        'form': PedidosClienteForm()
        }
    if request.method == "POST":
        formulario = PedidosClienteForm(request.POST)
        if formulario.is_valid():
            formulario.save()
             
    return render(request,'core/Recetauno.html', data)

def Recetados(request):
    data={
        'form': PedidosClienteForm()
    }
    if request.method == "POST":
        formulario = PedidosClienteForm(request.POST)
        if formulario.is_valid():
            formulario.save()
             
    return render(request,'core/Recetados.html', data)

def Recetatres(request):
    data={
        'form': PedidosClienteForm()
    }
    if request.method == "POST":
        formulario = PedidosClienteForm(request.POST)
        if formulario.is_valid():
            formulario.save()
             
    return render(request,'core/RecetaTres.html', data)

def CuentaCliente(request):
    return render(request,'core/CuentaCliente.html')

def ListadoPedidosCliente(request):
    pedidos = PedidosCliente.objects.all()
    data={
        'pedidos':pedidos
    }
    return render(request, 'core/ListadoPedidos.html', data)



def ModificarPedidos(request, id):
    pedido= PedidosCliente.objects.get(id=id)
    data={
        'form': PedidosClienteForm(instance=pedido)

    }

    if request.method == 'POST':
        formulario = PedidosClienteForm(data=request.POST, instance=pedido)
        if formulario.is_valid():
            formulario.save()
            
            data['form']= formulario
            return redirect('ListadoPedidosCliente')
    return render(request, 'core/ModificarPedidos.html' , data)


def EliminarPedido(request, id):
    pedido = PedidosCliente.objects.get(id=id)
    pedido.delete()
    return redirect(to="ListadoPedidosCliente")


def FormularioCompras(request):
    data={
        'form': PedidosClienteForm()
    }
    if request.method == "POST":
        formulario = PedidosClienteForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return redirect('home')
    return render(request, "core/FORMULARIOCOMPRA.html", data)




def Contacto(request):
    data={
        'form': FormularioContactoForm()
    }
    if request.method == "POST":
        formulario = FormularioContactoForm(request.POST)
        if formulario.is_valid():
            formulario.save()
            return redirect('home')
    return render(request, "core/FormularioContacto.html", data)

