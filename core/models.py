from django.db import models

class ModoPago(models.Model):
    nombre=models.CharField(max_length=50)
    def __str__(self):
        return self.nombre

class ListadoPoductos(models.Model):
    nombre=models.CharField(max_length=50)
    def __str__(self):
        return self.nombre


class PedidosCliente(models.Model):
    NombreCliente = models.CharField(max_length=50, verbose_name="Nombre Cliente")
    Apellidos = models.CharField(max_length=50)
    Telefono = models.CharField(max_length=50)
    Correo = models.CharField(max_length=50)
    Direccion = models.CharField(max_length=50)
    ModoPago = models.ForeignKey(ModoPago, on_delete=models.CASCADE, verbose_name="Modo Pago")
    NombreProducto= models.ForeignKey(ListadoPoductos, on_delete=models.CASCADE, verbose_name="Nombre Producto")


class FormularioContacto(models.Model):
    NombreCliente = models.CharField(max_length=50, verbose_name="Nombre Cliente")
    Apellidos = models.CharField(max_length=50)
    Telefono = models.CharField(max_length=50)
    Correo = models.CharField(max_length=50)
    Descripcion = models.CharField(max_length=50)
    def __str__(self):
        return self.NombreCliente
    