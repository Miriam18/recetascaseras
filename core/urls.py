from django.urls import path
from .views import base, home, Recetauno, Recetados, Recetatres, register, login, logout, CuentaCliente, ListadoPedidosCliente, ModificarPedidos, EliminarPedido, FormularioCompras,Contacto
from core import views

urlpatterns = [
    path('', base, name= "base"),
    path('home/', home, name= "home"),
    path('CuentaCliente/', CuentaCliente, name= "CuentaCliente"),
    path('BIZCOCHO_PARA_TORTAS/', Recetauno, name= "Recetauno"),
    path('MAGDALENAS_DE_YOGUR/', Recetados, name= "Recetados"),
    path('ROSQUILLAS_DE_ANIS/', Recetatres, name= "Recetatres"),
    path('register/', register, name= "register"),
    path('Listado_Productos/', ListadoPedidosCliente, name= "ListadoPedidosCliente"),
    path('Modificar_Pedidos/<id>/', ModificarPedidos, name= "ModificarPedidos"),
    path('Eliminar_Pedidos/<id>/', EliminarPedido, name= "EliminarPedido"),
    path('login/', login, name= "login"),
    path('logout/', logout, name= "logout"),
    path('FormularioCompras/', FormularioCompras, name= "FormularioCompras"),
    path('Contacto/', Contacto, name= "Contacto"),
    


]




