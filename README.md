# Pagina WEB ventas de Dulces Caseros
Esta pagina esta orientada a la venta de dulce caseros, contiene los modulos: inicio, quienes somos, Recetas Descatadas, Galeria, Contacto, inicio de sesion y Registro.

Para acceder a la compra de algun producto se debe registrar en la pagina mediante nombre de usuario, contraseña y confirmacion de la contraseña. si ya esta registrado, debe ingresar al modulo "Iniciar Sesion" .

Todos los datos ingresados quedarán guardados en la base de datos sqllite o en el administrador de django

# Vistas:
Se realizaron vistas para cada archivo html:

•	Vista Principal (base.html): Contiene los modulos inicio, quienes somos, Recetas Descatadas, Galeria, Contacto, inicio de sesion y Registro.

•	Vista Secundaria (home.html): Esta se puede visualizar luego de que se registra o logea el usuario. contiene los modeulos de inicio, quienes somos, Recetas Descatadas, Galeria, Contacto, Tu cuenta y Cerrar Sesion.


•	Vista Login(login.html): Contiene el formulario de login, en el se puede ingresar Nombre de Usuario y contraseña.

•	Vista Registro(registro.html): Contiene el formulario de registro, en el se puede ingresar Nombre de Usuario, contraseña y confirmacion de contraseña.


•	Vista Logout: Esta vista redirecciona al la Pagina Principal luego de seleccionar "Cerrar sesion".

•	Vista de Recetas: Visualiza las paginas de recetas que se seleccionan en la pagina HOME.


•	Vista Cuenta Cliente: Visualiza la informacion exclusiva del cliente y llama a la vista de Litado de Pedidos.

•	Vista de Listado de Pedidos: Visualiza el listado de todos los pedidos realizados por el usuario, y llama a la vista EliminarPedido y Modificar Pedido.


•	Vista Recuperar Contraseña: Contiene todos los html relacionados con la recuperacion de la contraseña.



# URL:
Se llamaron a cada una de las vistas mediante un url.

 Acceso Pagina Principal: http://127.0.0.1:8000/
                          http://localhost:8000/


 Acceso a Login: http://127.0.0.1:8000/accounts/login/
                 http://localhost:8000/accounts/login/


 Acceso a HOME: http://127.0.0.1:8000/home/
                http://localhost:8000/home/


 Acceso a Registro: http://127.0.0.1:8000/register/
                    http://localhost:8000/register/


 Acceso a Recetas: http://127.0.0.1:8000/BIZCOCHO_PARA_TORTAS/
                   http://localhost:8000/BIZCOCHO_PARA_TORTAS/

                   http://127.0.0.1:8000/MAGDALENAS_DE_YOGUR/
                   http://localhost:8000/MAGDALENAS_DE_YOGUR/

                   http://127.0.0.1:8000/ROSQUILLAS_DE_ANIS/
                   http://localhost:8000/ROSQUILLAS_DE_ANIS/


 Acceso a la cuenta personal del cliente: http://127.0.0.1:8000/CuentaCliente/
                                          http://localhost:8000/CuentaCliente/


 Acceso al administrador de django: http://127.0.0.1:8000/admin

 Acceso al Listado de Pedidos: http://127.0.0.1:8000/Listado_Productos/
                               http://localhost:8000/Listado_Productos/


 Acceso al modificar de Pedidos:http://127.0.0.1:8000/Modificar_Pedidos/4/
                                http://localhost:8000/Modificar_Pedidos/4/


 Acceso a recuperar contraseña: http://127.0.0.1:8000/password/recovery/
                                http://localhost:8000/password/recovery/

                
Formulario contacto:  http://127.0.0.1:8000/Contacto/
                      http://localhost:8000/Contacto/


Formulario de compras: http://127.0.0.1:8000/FormularioCompras/
                       http://localhost:8000/FormularioCompras/
# Modelos:

Se crearon 3 modelos:
-Modo Pago
-Listado Productos
-Pedidos Cliente



# RESULTADOS OBTENIDOS:
login, registro y logout  funcionan correctamente.
Ingreso de pedidos funcionan correctamente.
Formulario de contacto, integracion de facebook funcionan.



# se deben instalar para poder accedder a la pagina
para integracion de facebook: pip install social-auth-app-django
para crispy : pip install django_crispy_form

